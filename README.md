# The ATLAS model

The `methis.atlas` package main page is: [https://iarc-miarc.gitlab.io/methis/methis.atlas](https://iarc-miarc.gitlab.io/methis/methis.atlas)

The `ATLAS` model belong the the METHIS platform, which consists of a collection of models to predict the impact of prevention interventions against HPV-related cancer. For more information on the METHIS platform, please refer to the [METHIS website](https://iarc-miarc.gitlab.io/methis/methis.website/).


The ATLAS is a model to predict cohort specific cumulative cancer risk with and without targeted 
public health interventions.

This work is based on a publication by [Bonjour et al.](https://www.thelancet.com/journals/lanpub/article/PIIS2468-2667(21)00046-3/fulltext) 
work. Please refer to this article for more information on the model and its potential applications.

### Installation

In R:

``` r
remotes::install_gitlab('iarc-miarc/methis/methis.atlas', build_vignettes = TRUE)
```

### Usage

Please refer to the `atlas_lite` and `atlas_ui` help files, and the examples for how to use ATLAS.

```r
library(methis.atlas)
?atlas_lite
?atlas_ui
## open the getting started vignette
vignette('Getting_Started', package = 'methis.atlas')
## list of other available vignettes
vignette(package = 'methis.atlas')
```
